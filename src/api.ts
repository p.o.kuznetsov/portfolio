import axios from 'axios';
import {
  IUser,
  IAchievement,
  IUniversityInfo,
  IOrganizationIfno,
} from './interfaces';

const baseURL = process.env.VUE_APP_BASE_URL;

const client = axios.create({
  baseURL,
});

export async function getUser(): Promise<IUser> {
    const response = await client.get('/users/profile');
    return response.data as IUser;
}

export async function getAchievements(userId: number): Promise<IAchievement[]> {
    const response = await client.get(`users/${userId}/achievements`);
    return response.data as IAchievement[];
}

export async function getProjects(userId: number): Promise<IAchievement[]> {
  const response = await client.get(`users/${userId}/projects`);
  return response.data as IAchievement[];
}

export async function getUniversityInfo(userId: number): Promise<IUniversityInfo> {
  const response = await client.get(`users/${userId}/university`);
  return response.data as IUniversityInfo;
}

export async function getOrganizationInfo(userId: number): Promise<IOrganizationIfno> {
  const response = await client.get(`users/${userId}/organization`);
  return response.data as IOrganizationIfno;
}
