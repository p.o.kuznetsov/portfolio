import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';
import { getUser } from '@/api';
import { IUser } from '@/interfaces';

@Module({ name: 'UserModule' })
export default class UserModule extends VuexModule {
  public user: IUser = {
      id: 0,
      name: '',
      lastName: '',
      photoURL: '',
      email: '',
      phone: '',
  };

  @MutationAction({ mutate: ['user'] })
  public async fetchUser() {
    const user = await getUser();
    return { user };
  }
}
