import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';
import { IUniversityInfo } from '@/interfaces';
import { getUniversityInfo } from '@/api';


@Module({ name: 'GroupModule' })
export default class GroupModule extends VuexModule {
  public group: IUniversityInfo = {
      groupName: '',
      cathedralName: '',
      speciality: '',
      name: '',
      siteURL: '',
      labelURL: '',
      course: 0,
  };

  @MutationAction({ mutate: ['group'] })
  public async fetchGroups(userId: number) {
    const group = await getUniversityInfo(userId);
    return { group };
  }
}
