import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';
import { IOrganizationIfno } from '@/interfaces';
import { getOrganizationInfo } from '@/api';


@Module({ name: 'OrganizationModule' })
export default class OrganizationModule extends VuexModule {
  public organization: IOrganizationIfno = {
    name: '',
    description: '',
    organizationURL: '',
    position: '',
    labelURL: '',
  };

  @MutationAction({ mutate: ['organization'] })
  public async fetchOrganization(userId: number) {
    const organization = await getOrganizationInfo(userId);
    return { organization };
  }
}
