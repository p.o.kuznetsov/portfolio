import { IAchievement } from '@/interfaces';
import { Module, VuexModule, MutationAction } from 'vuex-module-decorators';
import { getAchievements } from '@/api';

@Module({ name: 'AchievementModule' })
export default class AchievementModule extends VuexModule {
  public achievements: IAchievement[] = [];

  @MutationAction({ mutate: ['achievements'] })
  public async fetchAchievements(userId: number) {
    const achievements = await getAchievements(userId);
    return { achievements };
  }
}
