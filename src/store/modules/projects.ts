import { Module, VuexModule, Mutation, MutationAction } from 'vuex-module-decorators';
import { IProject } from '@/interfaces';
import { getProjects } from '@/api';


@Module({ name: 'ProjectModule' })
export default class ProjectModule extends VuexModule {
  public projects: IProject[] = [];

  @MutationAction({ mutate: ['projects'] })
  public async fetchProjects(userId: number) {
    const projects = await getProjects(userId);
    return { projects };
  }
}
