import Vue from 'vue';
import Vuex from 'vuex';
import UserModule from './modules/users';
import OrganizationModule from './modules/organizations';
import GroupModule from './modules/groups';
import ProjectModule from './modules/projects';
import AchievementModule from './modules/achievements';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  modules: {
    UserModule,
    OrganizationModule,
    GroupModule,
    ProjectModule,
    AchievementModule,
  },
});
