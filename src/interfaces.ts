export interface IUniversityInfo {
    name: string;
    siteURL: string;
    labelURL: string;
    groupName: string;
    cathedralName: string;
    speciality: string;
    course: number;
}

export interface IOrganizationIfno {
    name: string;
    labelURL: string;
    description: string;
    organizationURL: string;
    position: string;
}

export interface IUser {
    id: number;
    name: string;
    lastName: string;
    photoURL: string;
    email: string;
    phone: string;
}

export interface IProject {
    id: number;
    title: string;
    description: string;
    shortDescription: string;
    image: string;
}

export interface IAchievement {
    id: number;
    date: string;
    title: string;
    description: string;
}

export interface ISettings {
    isEditMode: boolean;
}
