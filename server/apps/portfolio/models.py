from django.db import models

from server.apps.common.models.base import ORMCoreModel


class User(ORMCoreModel):
    name = models.CharField(max_length=16)
    last_name = models.CharField(max_length=32)
    photo_url = models.URLField()
    email = models.EmailField()
    phone = models.TextField()

    def __str__(self):
        return f'id={self.id}, name={self.name}'


class Project(ORMCoreModel):
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='projects')
    title = models.TextField()
    description = models.TextField()
    short_description = models.TextField()
    image = models.URLField()

    def __str__(self):
        return f'id={self.id}, title={self.title}'


class Achievement(ORMCoreModel):
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='achievements')
    date = models.TextField()
    title = models.TextField()
    description = models.TextField()

    def __str__(self):
        return f'id={self.id}, title={self.title}'


class OrganizationInfo(ORMCoreModel):
    user = models.OneToOneField('User', on_delete=models.CASCADE, related_name='organization')
    name = models.TextField()
    label_url = models.URLField()
    description = models.TextField()
    organization_url = models.URLField()
    position = models.TextField()

    def __str__(self):
        return f'id={self.id}, name={self.name}'


class UniversityInfo(ORMCoreModel):
    user = models.OneToOneField('User', on_delete=models.CASCADE, related_name='university')
    name = models.TextField()
    site_url = models.URLField()
    label_url = models.URLField()
    group_name = models.TextField()
    cathedral_name = models.TextField()
    speciality = models.TextField()
    course = models.IntegerField()

    def __str__(self):
        return f'id={self.id}, name={self.name}'
