from django.contrib import admin

from server.apps.portfolio import models


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Achievement)
class AchievementAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Project)
class ProjectAdmin(admin.ModelAdmin):
    pass


@admin.register(models.UniversityInfo)
class UniversityAdmin(admin.ModelAdmin):
    pass


@admin.register(models.OrganizationInfo)
class OrganizationAdmin(admin.ModelAdmin):
    pass
