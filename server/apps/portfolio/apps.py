# django
from django.apps import AppConfig


class PortfolioConfig(AppConfig):
    """Config for dms."""

    name = 'server.apps.portfolio'
