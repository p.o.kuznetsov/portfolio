from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from server.apps.api.v1 import serializers
from server.apps.common.viewsets.base import ViewSetMixin
from server.apps.portfolio import models


class PortfolioViewSet(ViewSetMixin, viewsets.ReadOnlyModelViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer
    request_serializer_classes = {
        'profile': serializers.UserSerializer,
        'projects': serializers.ProjectSerializer,
        'achievements': serializers.AchievementSerializer,
        'university': serializers.UniversitySerializer,
        'organization': serializers.OrganizationSerializer,
    }

    @action(methods=['get'], detail=False, url_path='profile')
    def profile(self, request, *args, **kwargs):
        """Get session details."""
        instance = self.queryset.first()
        serializer = self.get_serializer_class()(instance)
        return Response(serializer.data)

    @action(methods=['get'], detail=True, url_path='projects')
    def projects(self, request, *args, **kwargs):
        """Get session details."""
        instance = self.get_object()
        serializer = self.get_serializer_class()(instance.projects.all(), many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True, url_path='achievements')
    def achievements(self, request, *args, **kwargs):
        """Get session details."""
        instance = self.get_object()
        serializer = self.get_serializer_class()(instance.achievements.all(), many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True, url_path='university')
    def university(self, request, *args, **kwargs):
        """Get session details."""
        instance = self.get_object()
        serializer = self.get_serializer_class()(instance.university)
        return Response(serializer.data)

    @action(methods=['get'], detail=True, url_path='organization')
    def organization(self, request, *args, **kwargs):
        """Get session details."""
        instance = self.get_object()
        serializer = self.get_serializer_class()(instance.organization)
        return Response(serializer.data)
