from rest_framework import serializers

from server.apps.portfolio import models


class UserSerializer(serializers.ModelSerializer):
    photoURL = serializers.URLField(source='photo_url')

    class Meta:
        model = models.User
        fields = '__all__'


class UniversitySerializer(serializers.ModelSerializer):
    siteURL = serializers.URLField(source='site_url')
    labelURL = serializers.URLField(source='label_url')

    class Meta:
        model = models.UniversityInfo
        fields = '__all__'


class AchievementSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Achievement
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Project
        fields = '__all__'


class OrganizationSerializer(serializers.ModelSerializer):
    labelURL = serializers.URLField(source='label_url')
    organizationURL = serializers.URLField(source='organization_url')

    class Meta:
        model = models.OrganizationInfo
        fields = '__all__'
