# django
from django.apps import AppConfig


class ApiConfig(AppConfig):
    """Config for api."""

    name = 'server.apps.api'

    def ready(self):
        """On app ready."""
        from .v1 import signals  # noqa: WPS433, F401
