# built-in
import logging
from time import time
import re
from typing import Type

# django
from django.http import QueryDict
from rest_framework.response import Response as DRFResponse
from rest_framework.request import Request as DRFRequest


logger = logging.getLogger('django')

SENSITIVE_DATA = re.compile('api|secret|password|signature', re.I)
CLEANSED_SUBSTITUTE = '********'

IGNORED_PATHS = ()


class LoggerMiddleware:  # noqa: WPS306
    """Middleware for request/response logging."""

    def __init__(self, get_response):
        """Save get_response param."""
        self.get_response = get_response

    def __call__(self, request):
        """Logs request, response, query time."""
        t_start = time()
        log_request(request)
        response = self.get_response(request)

        log_response(request, response, time() - t_start)
        return response


def log_request(request: DRFRequest, with_body: bool = False):  # pragma: no cover
    """Logging request."""
    path = request.path
    if path in IGNORED_PATHS:
        return
    log_dict = {
        'method': request.method,
        'path': path,
        'remote_addr': request.META.get('REMOTE_ADDR'),
        'forwarded_for': request.META.get('HTTP_X_FORWARDED_FOR'),
    }
    logger.info({'REQUEST': log_dict})


def log_response(request: DRFRequest, response: DRFResponse, processing_time: float):  # pragma: no cover
    """Logging response."""
    path = request.path
    if path in IGNORED_PATHS:
        return
    log_dict = {
        'REQUEST': {
            'method': request.method,
            'path': path,
            'remote_addr': request.META.get('REMOTE_ADDR'),
            'forwarded_for': request.META.get('HTTP_X_FORWARDED_FOR'),
        },
        'RESPONSE': {'status_code': response.status_code},
        'TIME': processing_time,
    }
    if isinstance(response, DRFResponse):
        drf_request = getattr(response, 'renderer_context').get('request')
        log_dict['REQUEST'].update(  # type: ignore
            {
                'user_id': getattr(drf_request.user, 'id', None)
                if hasattr(drf_request, '_user') else None,  # noqa: WPS421
                'user_login': getattr(drf_request.user, 'login', None)
                if hasattr(drf_request, '_user') else None,  # noqa: WPS421
                'data': _get_request_data(drf_request),
                'query': _get_request_query(drf_request),
            },
        )
        log_dict['RESPONSE'].update(  # type: ignore
            {
                'view_class': getattr(response, 'renderer_context').get('view').__class__,
                'content': response.content,
            },
        )
    logger.info(log_dict)


def _get_request_data(request: DRFRequest):
    return _clean_data(request.data)  # type: ignore


def _get_request_query(request: DRFRequest) -> Type[QueryDict]:
    return _clean_data(request.query_params)  # type: ignore


def _clean_data(data_for_cleaning):  # pragma: no cover
    if not isinstance(data_for_cleaning, dict):
        return data_for_cleaning

    cleansed_data = {}

    for key in data_for_cleaning.keys():
        if SENSITIVE_DATA.search(key):
            cleansed_data[key] = CLEANSED_SUBSTITUTE
        else:
            cleansed_data[key] = data_for_cleaning[key]
    return cleansed_data
