# built-in
import logging

# django
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, exceptions, response as django_response
from rest_framework.views import exception_handler

# app
from server.apps.common.exceptions import WrappedValidationException, WrappedAPIException, ServiceBaseException

logger = logging.getLogger('django')


def _on_service_base_exception(exc, *args) -> django_response.Response:
    return exc.get_response()


def _on_validation_error(exc, *args) -> django_response.Response:
    return WrappedValidationException(exc).get_response()


def _on_api_exception(exc, *args) -> django_response.Response:
    return WrappedAPIException(exc).get_response()


def _on_object_not_exists(exc) -> django_response.Response:
    updated_exc = WrappedAPIException(exc)
    updated_exc.status = status.HTTP_404_NOT_FOUND
    updated_exc.message = 'Does not exists'
    return updated_exc.get_response()


exception_func = (
    (ServiceBaseException, _on_service_base_exception),
    (exceptions.ValidationError, _on_validation_error),
    (exceptions.APIException, _on_api_exception),
    (ObjectDoesNotExist, _on_object_not_exists),
)


def get_response(exc: Exception, context) -> django_response.Response:
    """Response from exception."""
    for instance, func in exception_func:
        if isinstance(exc, instance):
            return func(exc)  # type: ignore

    return exception_handler(exc, context)  # type: ignore
