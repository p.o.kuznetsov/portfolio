# built-in
import logging
import sys
import traceback

# django
from django.http import JsonResponse
from rest_framework import status, request as django_request
from rest_framework.views import set_rollback


logger = logging.getLogger('django')


def handler500(request: django_request.Request) -> JsonResponse:  # noqa: WPS210
    """Handler for 500."""
    set_rollback()

    tp, exc, tb = sys.exc_info()
    exception_details = {
        'excType': '{0}.{1}'.format(tp.__module__, tp.__name__),  # type: ignore  # noqa: WPS609
        'message': str(exc).splitlines(),
    }

    log_dict = {
        'REQUEST': {
            'method': request.method,
            'path': request.path,
            'user_id': request.user.id,
            'data': getattr(request, '_PERSISTENT_REQUEST_DATA', None),
        },
        'RESPONSE': {
            'status_code': 500,
            'view_class': request.resolver_match.view_name,
        },
        'EXCEPTION': exception_details,
        'TRACEBACK': traceback.format_list(traceback.extract_tb(tb)),
    }
    logger.warning(log_dict)

    # TODO: update with common error codes design
    resp_data = {
        'error': {
            'statusCode': status.HTTP_500_INTERNAL_SERVER_ERROR,
            'errorCode': 'INTERNAL_SERVER_ERROR_500',
            'message': 'Internal Server Error',
            'details': exception_details,
        },
    }

    return JsonResponse(resp_data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
