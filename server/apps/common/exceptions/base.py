# typing
from typing import Any

# django
from rest_framework.response import Response


class ServiceBaseException(Exception):
    """Base Exception for app."""

    status = 500
    scope = 'UNDEFINED'
    code = 0
    message = 'Generic error w/o detail'

    def __init__(self, **kwargs: Any):
        """Set kwargs for response."""
        self._kwargs = kwargs

    def get_response(self) -> Response:
        """Prepares response."""
        return Response({'error': self.error_body}, status=self.status)

    @property
    def error_body(self):
        """."""
        return {
            'statusCode': self.status,
            'errorCode': '{0}_{1}'.format(self.scope, self.code),
            'message': self.message,
            'payload': self._kwargs,
        }

    def __str__(self):  # pragma: no cover
        return str(self.error_body)
