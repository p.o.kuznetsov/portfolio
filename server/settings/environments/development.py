# built-in
import logging
from typing import List

# app
from server.settings.components.apps import INSTALLED_APPS
from server.settings.components.common import MIDDLEWARE
from server.settings.components.drf import REST_FRAMEWORK

# Setting the development status:

DEBUG = True

ALLOWED_HOSTS = ['*']

STATICFILES_DIRS: List[str] = []

INSTALLED_APPS += (
    'debug_toolbar',
    'nplusone.ext.django',
)

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'querycount.middleware.QueryCountMiddleware',
)

MIDDLEWARE = ('nplusone.ext.django.NPlusOneMiddleware',) + MIDDLEWARE

NPLUSONE_RAISE = True
NPLUSONE_LOGGER = logging.getLogger('django')
NPLUSONE_LOG_LEVEL = logging.WARN
NPLUSONE_WHITELIST = [
    # As n+1 is very unhappy with Django-admin:
    {'model': 'admin.LogEntry', 'field': 'user'},
    {'model': 'accounts.User', 'field': 'groups'},
    # TODO: And this possibly requires improvements:
    {'model': 'dms.ShiftResource'},
    {'model': 'dms.Shift'},
    {'model': 'dms.IntervalPriceBasket'},
    {'model': 'dms.VendorSegmentRule'},
    {'model': 'estimator.EstimatorVehicle'},
    {'model': 'estimator.EstimatorStop'},
]


DEBUG_TOOLBAR_PANELS = [
    'ddt_request_history.panels.request_history.RequestHistoryPanel',  # Here it is
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]

DEBUG_TOOLBAR_CONFIG = {
    'RESULTS_STORE_SIZE': 100,
}

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] += (  # type: ignore
    'rest_framework.renderers.BrowsableAPIRenderer',
)

TEST_RUNNER = 'snapshottest.django.TestRunner'
