# built-in
from typing import Tuple


INSTALLED_APPS: Tuple[str, ...] = (
    # Default django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    # django-admin:
    'django.contrib.admin',
    'django.contrib.admindocs',
    # drf
    'rest_framework',
    'drf_yasg',
    'django_filters',
    # cors
    'corsheaders',
    # Your apps go here:
    'server.apps.portfolio',
    'server.apps.common',
)
