psql << EOF
    CREATE USER "portfolio" WITH PASSWORD 'portfolio';
    ALTER ROLE "portfolio" SUPERUSER;
    CREATE DATABASE "portfolio";
    GRANT ALL PRIVILEGES ON DATABASE "portfolio" to "portfolio";
EOF
