psql -d delivery-management-system << EOF
    CREATE EXTENSION "uuid-ossp";
    CREATE EXTENSION "postgis";
    CREATE EXTENSION "postgis_topology";
    CREATE EXTENSION "fuzzystrmatch";
    CREATE EXTENSION "postgis_tiger_geocoder";
EOF
